import sys
import os
import shutil
from datetime import datetime,timezone
import filecmp
import time
import argparse
import hashlib

class synchronize_folders:

    def __init__(self,
            in_folder_src,
            in_folder_dst,
            in_folder_config,
            in_sleep_interval=0  # We don't need loop for subfolders
        ):

        self.folder_src     = in_folder_src
        self.folder_dst     = in_folder_dst
        self.folder_config  = in_folder_config
        self.sleep_interval = in_sleep_interval

        self.file_log       = os.path.join(in_folder_config, 'synchronize_folders_log.txt')
        self.file_config    = os.path.join(in_folder_config, 'synchronize_folders_config.txt')

    def print_message(self,in_message):
        
        curr_date_time  = datetime.now(timezone.utc).strftime('%d-%m-%Y %H:%M:%S')
        curr_message    = curr_date_time + ' - ' + str(in_message)
        print(curr_message)

        try:
            with open(self.file_log, 'a') as temp_f:
                print(curr_message, file=temp_f)
        except:
            print(curr_date_time,'cannot access to log file: ', self.file_log)
            print(curr_date_time,sys.exc_info()[1])

    def delete_object(self,in_path):

        curr_message = in_path

        if not os.path.exists(in_path):
            self.print_message('cannot find ' + in_path)
            return True

        try:
            if os.path.isfile(in_path):
                os.remove(in_path)
                self.print_message('deleted file: ' + curr_message)
            
            elif os.path.isdir(in_path):
                shutil.rmtree(in_path)
                self.print_message('deleted folder: ' + curr_message)
            else:
                self.print_message('uknown type of path: ' + curr_message)
                return False
        except:
            self.print_message('cannot delete ' + curr_message)
            self.print_message(sys.exc_info()[1])
            return False

        return True

    def delete_extra(self):

        list_src = os.listdir(self.folder_src)
        list_dst = os.listdir(self.folder_dst)

        list_diff = list(set(list_dst) - set(list_src))
        
        for curr_object_name in list_diff:
            
            curr_object_path  = os.path.join(self.folder_dst, curr_object_name)

            self.delete_object(curr_object_path)

    def check_folder(self,in_folder):

        if not os.path.exists(in_folder):
            self.print_message('cannot find ' + in_folder)
            return False
        return True

    def check_folders(self):
        
        curr_res = True

        curr_res = curr_res and self.check_folder(self.folder_src)
        curr_res = curr_res and self.check_folder(self.folder_dst)
        curr_res = curr_res and self.check_folder(self.folder_config)

        return curr_res

    def get_hash(self,in_object_path):
        
        curr_hasher = hashlib.sha256()  # It's better to take from the config file
        curr_hash_size = 4096           # It's better to take from the config file

        try:
            with open(in_object_path,'rb') as temp_f:

                while True:
                    curr_buffer = temp_f.read(curr_hash_size)
                    if not curr_buffer:
                        break
                    curr_hasher.update(curr_buffer)
        except:
            self.print_message('cannot get hash for ' + in_object_path)
            self.print_message(sys.exc_info()[1])


        return curr_hasher.hexdigest()

    def check_hash(self,in_object_src_path, in_object_dst_path):

        curr_hash_src = self.get_hash(in_object_src_path)
        curr_hash_dst = self.get_hash(in_object_dst_path)
        
        if curr_hash_src == curr_hash_dst:
            return True
        
        return False

    def files_equal(self,in_object_src_path,in_object_dst_path):

        # It's not clear enough what exactly means 'identical' in this task,
        # how huge files are being synchronized and how lot of them are used.
        # So, I just show that I know different ways to compare two files.
        # Ideally is to set the necessary way in the config file

        if not self.check_hash(in_object_src_path, in_object_dst_path):
            return False

        # if not filecmp.cmp(in_object_src_path, in_object_dst_path, True):
        #    return False

        # if not filecmp.cmp(in_object_src_path, in_object_dst_path, False):
        #    return False
        
        return True

    def process_file(self,in_file_name):
        
        curr_object_src_path  = os.path.join(self.folder_src, in_file_name)
        curr_object_dst_path  = os.path.join(self.folder_dst, in_file_name)

        curr_message = ' file ' + curr_object_src_path + ' to ' + curr_object_dst_path

        if os.path.exists(curr_object_dst_path):
            
            if os.path.isfile(curr_object_dst_path):
                if self.files_equal(curr_object_src_path,curr_object_dst_path):
                    return
            else: 
                if not self.delete_object(curr_object_dst_path): 
                    self.print_message('cannot copy' + curr_message)
                    return
        try:
            shutil.copy2(curr_object_src_path, curr_object_dst_path)
            self.print_message('copied' + curr_message)
        except:
            self.print_message('cannot copy' + curr_message)
            self.print_message(sys.exc_info()[1])

    def process_folder(self,in_folder_name):

        curr_object_src_path  = os.path.join(self.folder_src, in_folder_name)
        curr_object_dst_path  = os.path.join(self.folder_dst, in_folder_name)

        curr_message = ' folder ' + curr_object_src_path + ' to ' + curr_object_dst_path

        if (os.path.exists(curr_object_dst_path) and not os.path.isdir(curr_object_dst_path)):

            if not self.delete_object(curr_object_dst_path): 
                self.print_message('cannot copy' + curr_message)
                return
                
        if not os.path.exists(curr_object_dst_path):
            try:
                shutil.copytree(curr_object_src_path,curr_object_dst_path)
                self.print_message('copied' + curr_message)
            except:
                self.print_message('cannot copy' + curr_message)
                self.print_message(sys.exc_info()[1])
            return
        
        my_synch = synchronize_folders(  # We don't need loop for subfolders
            curr_object_src_path,
            curr_object_dst_path,
            self.folder_config
        )

        my_synch.process_all()

    def update_all(self):
        
        curr_folders_list = [] # Processing files before folders for more user-friendly log

        with os.scandir(self.folder_src) as curr_objects:
            
            for curr_object in curr_objects:

                if curr_object.is_dir():
                    curr_folders_list.append(curr_object)
                
                elif curr_object.is_file():
                    self.process_file(curr_object.name)

            for curr_folder in curr_folders_list:
                self.process_folder(curr_folder.name)

    def process_all(self):

        self.delete_extra()
        self.update_all()

    def check_stop(self):
        try:
            with open(self.file_config, 'r') as temp_f:
                for curr_line in temp_f:
                    if curr_line == 'stop':
                        return True
        except:
            temp_message = 'It is OK' # Config file wasn't in the task, anyway
        return False

    def process_all_root(self):

        self.print_message('============================================================')
        
        if not self.check_folders():
            self.print_message('synchronizing canceled')
            return

        self.print_message('synchronizing started')

        filecmp.clear_cache() # It should depend on config file: is it necessary or not

        self.process_all()

        self.print_message('synchronizing finished')

    def start(self):

        while True:
            if self.check_stop():
                return

            self.process_all_root()
            
            if self.sleep_interval==0: # let's run only once if the interval wasn't set
                break

            if self.check_stop():   # Processing could last for a long,
                return              # so, it's better to check again before sleep
            
            time.sleep(self.sleep_interval)

def createParser ():
    parser = argparse.ArgumentParser()
    
    parser.add_argument ('-s', '--folder_src'   ,required=True)
    parser.add_argument ('-d', '--folder_dst'   ,required=True)
    parser.add_argument ('-c', '--folder_config',required=True)
    
    parser.add_argument ('-i', '--sleep_interval', type=int, default = 0) 
            # if sleep_interval is not set, it means to run script once
    return parser

if __name__ == '__main__':

    temp_debug = False

    if temp_debug:              # It's kind of constructor for future work, not commercial product. 
        temp_folder_src_init    = 'D:\\marcus\\delme\\1\\'      # So, the debug mode switch
        temp_folder_dst_init    = 'D:\\marcus\\delme\\2\\'      # could stay for future development,
        temp_folder_config      = 'D:\\marcus\\delme\\config\\' # I guess
        temp_sleep_interval     = 10
    else:
        parser = createParser()
        namespace = parser.parse_args(sys.argv[1:])
    
        temp_folder_src_init    = namespace.folder_src
        temp_folder_dst_init    = namespace.folder_dst
        temp_folder_config      = namespace.folder_config       # I've decided to use a config folder 
                                                                # instead of a config file
        temp_sleep_interval     = abs(namespace.sleep_interval)

    my_synch = synchronize_folders(
        temp_folder_src_init,
        temp_folder_dst_init,
        temp_folder_config,
        temp_sleep_interval
    )
        
    my_synch.start()